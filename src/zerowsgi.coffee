http = require 'http'
msgpack = require 'msgpack'
optionsparser = require 'options-parser'
uuid = require 'node-uuid'
zmq = require 'zmq'

main = ->
  options = optionsparser.parse({
    'help' :       { short: 'h', flag: true },
    'http-listen': { default: '127.0.0.1:8000' },
    'push-listen': { default: 'ipc:///tmp/zerowsgi-push' },
    'pull-listen': { default: 'ipc:///tmp/zerowsgi-pull' },
    'timeout':     { default: 60000 }
  })
  
  if options.opt.help
    console.log 'You asked for help, you got help.'
    console.log options
    process.exit 0

  config = {
    'pushListen': options.opt['push-listen'],
    'pullListen': options.opt['pull-listen'],
    'responseTimeout': options.opt['timeout']
  }

  httpListen = options.opt['http-listen'].split(':')
  config.httpHost = httpListen[0]
  config.httpPort = httpListen[1]

  router = new ZeroWsgiRouter config
  router.start()

class ZeroWsgiRequest
  constructor: (@router, @req, @res) ->
    @id = uuid.v4()
    @router.addRequest @
    @timeout = setTimeout @timeout, @router.config.responseTimeout
    @body = ''

  pack: ->
    packet = {
      requestId: @id,
      headers: @req.headers,
      url: @req.url,
      method: @req.method,
      post: @body
    }
    return msgpack.pack packet

  processResponse: (response) ->
    clearTimeout(@timeout)
    @res.writeHead response.status, response.headers
    @res.write response.body
    @finish()
  
  timeout: =>
    @res.writeHead 200, {'Content-Type': 'text/plain'}
    @res.write 'Application timeout'
    @finish()

  finish: ->
    @res.end()
    @router.deleteRequest @id

  send: =>
    @req.on('data', (chunk) =>
      @body += chunk
    )

    @req.on('end', =>
      @router.push.send(@pack())
    )

class ZeroWsgiRouter
  constructor: (@config) ->
    @requests = {}
  
  addRequest: (requestObject) ->
    @requests[requestObject.id] = requestObject

  deleteRequest: (requestId) ->
    if @requests[requestId]?
      delete @requests[requestId]

  start: ->
    @push = zmq.socket 'push'
    @push.bindSync @config.pushListen

    @pull = zmq.socket 'pull'
    @pull.bindSync @config.pullListen

    @pull.on('message', (message) =>
      response = msgpack.unpack message
      requestObject = @requests[response.requestId]
      if not requestObject?
        return
      requestObject.processResponse response
    )

    @web = http.createServer (req, res) =>
      request = new ZeroWsgiRequest @, req, res
      request.send()
    @web.listen @config.httpPort, @config.httpHost

main()

